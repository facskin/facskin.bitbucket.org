#!/bin/sh

cd src

rm -f *~

VERSION=`cat version`
RELEASEDATE=`cat releasedate`

for f in *; do
  if ! ( [ "$f" == "header" ] || [ "$f" == "footer" ] || [ "$f" == "version" ] || [ "$f" == "releasedate" ] ); then
    echo $f

    TITLE=`head -n 1 $f`

    cat header           >../$f.html
    cat $f | tail -n +2 >>../$f.html
    cat footer          >>../$f.html
    sed -i -e "s/\@\@\@version\@\@\@/$VERSION/g"         ../$f.html
    sed -i -e "s/\@\@\@releasedate\@\@\@/$RELEASEDATE/g" ../$f.html
    sed -i -e "s/\@\@\@title\@\@\@/$TITLE/g"             ../$f.html
  fi
done
